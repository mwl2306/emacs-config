(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-by-copying t)
 '(backup-directory-alist '(("." . "~/.emacs_backups")))
 '(company-idle-delay 0.1)
 '(debug-on-error nil)
 '(global-hl-line-mode t)
 '(highlight-indent-guides-method 'character)
 '(inhibit-startup-screen t)
 '(lsp-enable-semantic-highlighting t)
 '(lsp-vhdl-server 'vhdl-ls)
 '(org-outline-path-complete-in-steps nil)
 '(org-refile-allow-creating-parent-nodes 'confirm)
 '(org-refile-use-outline-path t)
 '(org-roam-db-location "/home/micah/org-roam/org-roam.db")
 '(org-roam-graph-executable "fdp")
 '(org-roam-graph-max-title-length 50)
 '(org-roam-graph-shorten-titles 'wrap)
 '(package-archives
   '(("org" . "https://orgmode.org/elpa/")
     ("melpa" . "https://melpa.org/packages/")
     ("gnu" . "https://elpa.gnu.org/packages/")))
 '(package-selected-packages
   '(rainbow-delimiters yasnippet helm-notmuch hydra helm company all-the-icons notmuch helm-lsp helm-c-yasnippet yasnippet-snippets toml-mode company-org-roam org-mode projectile posframe pdf-tools org-roam org-journal org-download nov neotree multiple-cursors magit lsp-ui lsp-treemacs leaf-keywords highlight-indent-guides graphviz-dot-mode flycheck el-get company-lsp clang-format+ centaur-tabs ccls bui blackout bind-key beacon adoc-mode))
 '(send-mail-function 'smtpmail-send-it)
 '(smtpmail-default-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(vhdl-standard '(8 nil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
