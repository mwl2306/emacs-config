;;--------------------------------------------------------------------------------
;; init.el - trying out use-package
;;--------------------------------------------------------------------------------
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(setq custom-file  "~/.emacs.d/custom_file.el")
(load custom-file)


;;--------------------------------------------------------------------------------
(use-package all-the-icons
  :ensure t)
;;--------------------------------------------------------------------------------
(use-package zenburn-theme
  :ensure t
  :config
  (load-theme 'zenburn t)
  )
;;--------------------------------------------------------------------------------
(use-package beacon
  :ensure t
  :custom
  (beacon-blink-when-window-scrolls t)
  :config
  (beacon-mode 1)
  )
;;--------------------------------------------------------------------------------
(use-package company
  :ensure t
  :hook (after-init . global-company-mode)
  )

(use-package company-lsp
  :ensure t
  :commands company-lsp
  :config (push 'company-lsp company-backends)
  )
;;--------------------------------------------------------------------------------
(use-package flycheck
  :ensure t)
;;--------------------------------------------------------------------------------
(use-package helm
  :ensure t
  :config
  (helm-mode 1)
  (require 'helm-config)
  :bind (("C-x C-f" . helm-find-files)
	 ("C-c h o" . helm-occur))
  )
;;--------------------------------------------------------------------------------
(use-package highlight-indent-guides
  :ensure t
  :custom (highlight-indent-guides-method 'bitmap)
  :hook (prog-mode . highlight-indent-guides-mode)
  )
;;--------------------------------------------------------------------------------
(use-package lsp-mode
  :ensure t
  :hook ((c-mode . lsp)
	 (vhdl-mode . lsp))
  :custom ;;enable rust_hdl/flycheck lsp features
  (lsp-vhdl-server-path "~/projs/rust_hdl/target/release/vhdl_ls") 
  )

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)


(use-package helm-lsp
  :ensure t
  :commands helm-lsp-workspace-symbol
  :config
  (define-key lsp-mode-map [remap xref-find-apropos] #'helm-lsp-workspace-symbol)
  )

(use-package ccls
  :ensure t
  :config
  (setq ccls-executable "/usr/bin/ccls")
  (setq lsp-prefer-flymake nil)
  (setq-default flycheck-disabled-checkers '(c/c++-clang c/c++-cppcheck c/c++-gcc))
  )
;;--------------------------------------------------------------------------------
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)
	 ("C-x M-g" . magit-dispatch))
  )
;;--------------------------------------------------------------------------------
(use-package multiple-cursors
  :ensure t
  :bind (("C-S-c C-S-c" . mc/edit-lines))
  )
;;--------------------------------------------------------------------------------
(use-package neotree
  :ensure t
  :bind (("<f8>" . neotree-toggle))
  )
;;--------------------------------------------------------------------------------
;;org stuff
;;--------------------------------------------------------------------------------
(use-package org
  :ensure t
  :custom
  (org-ellipsis " ✦✦✦")
  ;;------------------------------------------------------------------------------
  (org-directory "~/Dropbox/org")
  ;;------------------------------------------------------------------------------
  (org-capture-templates
   `(("i" "inbox" entry (file ,(concat org-directory "/inbox.org"))
      "* TODO %?")
      ("c" "org-protocol-capture" entry (file ,(concat org-directory "/inbox.org"))
       "* TODO [[%:link][%:description]]\n\n %i" :immediate-finish t)))
  ;;------------------------------------------------------------------------------
  (org-todo-keywords
   '((sequence "TODO(t)" "WAITING(w)" "NEXT(n)" "TICKLER(i)" "SOMEDAY(s)" "PROJECT(p)" "RAY(r)" "|" "DONE(d)" "CANCELLED(c)")))
  ;;------------------------------------------------------------------------------
  (org-agenda-files `(,(concat org-directory "/projects.org")
		      ,(concat org-directory "/next.org")
		      ,(concat org-directory "/tickler.org")
		      ,(concat org-directory "/waiting.org")))
  ;;------------------------------------------------------------------------------
  (org-refile-targets `((,(concat org-directory "/projects.org") :maxlevel . 5)
			(,(concat org-directory "/waiting.org") :maxlevel . 5)
			(,(concat org-directory "/someday.org") :maxlevel . 5)
			(,(concat org-directory "/tickler.org") :maxlevel . 5)
			(,(concat org-directory "/reference.org") :maxlevel . 5)
			(,(concat org-directory "/next.org") :maxlevel . 5)
			(,(concat org-directory "/ray.org") :maxlevel . 5)))
  ;;------------------------------------------------------------------------------
  (org-refile-use-outline-path 'file)
  ;;------------------------------------------------------------------------------
  (org-default-notes-file (concat org-directory "inbox.org"))
  ;;------------------------------------------------------------------------------
  (org-archive-location (concat org-directory "/archives/%s_archive::"))
  ;;------------------------------------------------------------------------------
  (org-log-done 'note)
  ;;------------------------------------------------------------------------------
  :hook (org-mode . org-indent-mode)
  :config
  (require 'org)
  ;;----------------------------------------
  (load "server")
  (unless (server-running-p) (server-start))
  ;;----------------------------------------
  (require 'org-protocol) 
  )
;;--------------------------------------------------------------------------------
(use-package hydra
  :ensure t)
(use-package org-fc
  :load-path "tools/org-fc"
  :custom (org-fc-directories '(org-directory))
  :config
  (require 'org-fc-hydra))
;;--------------------------------------------------------------------------------
;;org-roam
;;--------------------------------------------------------------------------------
(use-package org-roam
  :ensure t
  :hook
  (after-init . org-roam-mode)
  :custom
  (org-roam-directory "/home/micah/org-roam")
  :bind (:map org-roam-mode-map
          (("C-c n l" . org-roam)
           ("C-c n f" . org-roam-find-file)
           ("C-c n g" . org-roam-graph)
	   ("C-c n b" . org-roam-buffer-activate))
          :map org-mode-map
          (("C-c n i" . org-roam-insert))
          (("C-c n I" . org-roam-insert-immediate)))
  :config
  (setq org-roam-graph-extra-config '(("splines" . "curved")))
  ;;------------------------------------------------------------------------------
  (setq org-roam-graph-node-extra-config '(("shape" . "ellipse")
					   ("style" . "filled")
					   ("color" . "black")
					   ("fillcolor" . "lightcyan2")))
  ;;------------------------------------------------------------------------------
  (require 'org-roam-protocol)
  )

(use-package company-org-roam
  :ensure t
  :config
  (push 'company-org-roam company-backends)
  )

;;--------------------------------------------------------------------------------
(use-package projectile
  :ensure t
  :bind-keymap
  ("C-c p" . projectile-command-map)
  )
;;--------------------------------------------------------------------------------
(use-package toml-mode
  :ensure t)
;;--------------------------------------------------------------------------------
(use-package yasnippet
  :ensure t
  :hook (prog-mode . yas-minor-mode)
  :config
  (yas-reload-all)
  )

(use-package yasnippet-snippets
  :ensure t)

(use-package helm-c-yasnippet
  :after (yasnippet)
  :ensure t
  :config (setq helm-yas-space-match-greedy t)
  :bind ("C-c y" . helm-yas-complete)
  )
;;--------------------------------------------------------------------------------
(use-package notmuch
  :ensure t)

(use-package helm-notmuch
  :ensure t)
;;--------------------------------------------------------------------------------
(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode)
  )

;;Miscellaneous Stuff I'm not sure how to enable with use-package
;;(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)

(add-hook 'prog-mode-hook 'linum-mode)

